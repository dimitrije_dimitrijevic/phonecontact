import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {UserItem} from "../../model/userItem";

@Injectable()
export class ContactProvider {

  constructor(private storage: Storage) {
  }

  createContact(user: UserItem): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      let contacts: Array<UserItem> = [];
      this.storage.get('contcts').then(value => {
        contacts = value;
        user.id = Math.floor(Math.random() * 99999999);
        contacts.push(user);
        this.storage.set('contcts', contacts);
        resolve(true);
      }).catch(() => {
        contacts = [];
        user.id = Math.floor(Math.random() * 99999999);
        contacts.push(user);
        this.storage.set('contcts', contacts);
        reject(false);
      });
    });
  }

  editContact(user: UserItem): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      let contacts: Array<UserItem> = [];
      this.storage.get('contcts').then(value => {
        contacts = value;
        let newContacts = contacts.map((item) => {
          if (item.id === user.id) {
            item.lastName = user.lastName;
            item.name = user.name;
            item.phone = user.phone;
          }
          return item;
        });
        this.storage.set('contcts', newContacts);
        resolve(true);
      }).catch(() => {
        reject(false);
      });
    });
  }

  deleteContact(user: UserItem): Promise<Array<UserItem>> {
    return new Promise<Array<UserItem>>((resolve, reject) => {
      let contacts: Array<UserItem> = [];
      this.storage.get('contcts').then(value => {
        contacts = value;
        let newContacts = contacts.filter((item) => {
          if (item.id !== user.id)
            return item;
        });
        this.storage.set('contcts', newContacts);
        resolve(newContacts);
      }).catch(() => {
        reject(null);
      });
    });
  }

  getAllContact(): Promise<Array<UserItem>> {
    return new Promise<Array<UserItem>>((resolve, reject) => {
      let contacts: Array<UserItem> = [];
      this.storage.get('contcts').then(value => {
        contacts = value;
        resolve(contacts);
      }).catch(() => {
        reject(null);
      });
    });
  }

}
