export interface UserItem {
  id?: number;
  name: string;
  lastName?: string;
  phone?: string;
}
