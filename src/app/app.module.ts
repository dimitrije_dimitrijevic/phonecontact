import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import {ListContactPageModule} from "../pages/list-contact/list-contact.module";
import {EditContactPageModule} from "../pages/edit-contact/edit-contact.module";
import {CreateContactPageModule} from "../pages/create-contact/create-contact.module";
import { ContactProvider } from '../providers/contact/contact';
import {IonicStorageModule} from "@ionic/storage";
import { ToastMessageProvider } from '../providers/toast-message/toast-message';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__contacts',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    ListContactPageModule,
    EditContactPageModule,
    CreateContactPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ContactProvider,
    ToastMessageProvider
  ]
})
export class AppModule {}
