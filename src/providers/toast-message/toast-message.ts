import { Injectable } from '@angular/core';
import {ToastController} from "ionic-angular";

@Injectable()
export class ToastMessageProvider {


  constructor(public toastCtrl: ToastController) {
  }

  public showToast(message: string): void {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
