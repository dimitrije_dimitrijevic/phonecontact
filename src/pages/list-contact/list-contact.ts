import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {EditContactPage} from "../edit-contact/edit-contact";
import {UserItem} from "../../model/userItem";
import {CreateContactPage} from "../create-contact/create-contact";
import {ContactProvider} from "../../providers/contact/contact";
import {ToastMessageProvider} from "../../providers/toast-message/toast-message";

@IonicPage()
@Component({
  selector: 'page-list-contact',
  templateUrl: 'list-contact.html',
})
export class ListContactPage {

  allContact: Array<UserItem> = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private contactProvider: ContactProvider,
              private toast: ToastMessageProvider) {


  }

  ionViewDidLoad() {

  }

  ionViewWillEnter() {
    this.contactProvider.getAllContact().then((contacts) => {
      this.allContact = contacts;
    }, err => {
      this.allContact = [];
      this.toast.showToast("An error has occurred");
    });
  }

  editContact(user: UserItem) {
    this.navCtrl.push(EditContactPage, {editUser: user})
  }

  deleteContact(user: UserItem) {
    const confirm = this.alertCtrl.create({
      title: 'Delete ' + user ? `${user.name} ${user.lastName}` : '',
      message: 'Are your sure?',
      buttons: [
        {
          text: 'No'
        },
        {
          text: 'Yes',
          handler: () => {
            this.contactProvider.deleteContact(user).then((contacts) => {
              this.allContact = contacts;
            }, err => {
              this.allContact = [];
              this.toast.showToast("An error has occurred");
            });
          }
        }
      ]
    });
    confirm.present();
  }


  createContact() {
    this.navCtrl.push(CreateContactPage)
  }


  doRefresh(refresher) {
    setTimeout(() => {
      this.contactProvider.getAllContact().then((contacts) => {
        this.allContact = contacts;
      }, err => {
        this.allContact = [];
        this.toast.showToast("An error has occurred");
      });
      refresher.complete();
    }, 500);
  }
}
