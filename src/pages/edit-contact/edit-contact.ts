import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserItem} from "../../model/userItem";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastMessageProvider} from "../../providers/toast-message/toast-message";
import {ContactProvider} from "../../providers/contact/contact";

@IonicPage()
@Component({
  selector: 'page-edit-contact',
  templateUrl: 'edit-contact.html',
})
export class EditContactPage {

  editContact: UserItem;
  editForm: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              private contactProvider: ContactProvider,
              private toast: ToastMessageProvider) {
    this.editContact = this.navParams.get('editUser');
    this.editForm = this.formBuilder.group({
      id: [this.editContact.id],
      name: [this.editContact.name, [Validators.required]],
      lastName: [this.editContact.lastName, []],
      phone: [this.editContact.phone, []]
    });
  }

  ionViewDidLoad() {
  }

  backArrow() {
    this.navCtrl.pop();
  }

  saveContact(): void {
    let user: UserItem = this.editForm.value;
    this.contactProvider.editContact(user).then((contacts) => {
      this.toast.showToast("Success edit contact");
      this.navCtrl.pop();
    }, err => {
      this.toast.showToast("An error has occurred");
    });
  }

}
