import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastMessageProvider} from "../../providers/toast-message/toast-message";
import {ContactProvider} from "../../providers/contact/contact";
import {UserItem} from "../../model/userItem";

@IonicPage()
@Component({
  selector: 'page-create-contact',
  templateUrl: 'create-contact.html',
})
export class CreateContactPage {

  createForm: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              private contactProvider: ContactProvider,
              private toast: ToastMessageProvider) {
    this.createForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      lastName: ['', []],
      phone: ['']
    });
  }

  ionViewDidLoad() {

  }

  createContact(): void {
    let user: UserItem = this.createForm.value;
    this.contactProvider.createContact(user).then((contacts) => {
      this.toast.showToast("Success add contact");
      this.navCtrl.pop();
    }, err => {
      this.toast.showToast("An error has occurred");
      this.navCtrl.pop();
    });
  }

  backArrow() {
    this.navCtrl.pop();
  }

}
